package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created as a test on 20/11/2017.
 */
public class Test01_login extends BaseTest {
    @Test
    public void testLogin (){
        //Step1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 click on the Login link and the Login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser", "testing"));

   //step4 Logout
        assertTrue(userPage.clickOnlogout(welcomePage));
    }
}
